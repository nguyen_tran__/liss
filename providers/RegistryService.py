# -*- coding: utf-8 -*-
"""
Created on Wed May 24 12:33:34 2017

@author: Nguyen Tran
Registry service that stores registered services of providers involving in LiSS
in run time
"""

from Abstract.Abstract import AbsRegistry

class Registry(AbsRegistry):
    def __init__(self, moduleParams, dependencies):
        super(AbsRegistry, self).__init__(moduleParams = moduleParams, dependencies = dependencies, registry = None)
