# -*- coding: utf-8 -*-
"""
Created on Fri May 26 14:52:58 2017

@author: Nguyen Tran
"""
import Abstracts
import MongoDBProvider
import ScopusPrimarySearcher
import ScopusSnowballingSearcher
import Queue
from fuzzywuzzy import fuzz

import requests
import json        
try:
    from urlparse import urlparse
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urlparse
    from urllib.parse import urljoin

class LiSSProvider(Abstracts.AbsLiSSProvider):
    def __init__(self, **kwargs):
        kwargs["providerName"] = "LiSS"
        super(LiSSProvider, self).__init__(**kwargs)
    
    def _openTopic(self, inputQueue, outputQueue, topic):
        try:
            if topic.entityType is not "LiSS_TOPIC":
                response = Abstracts.Response("The given topic is invalid")
                outputQueue.put(response)
        except AttributeError:
            response = Abstracts.Response("The given topic is invalid")
            outputQueue.put(response)
            
        databaseProvider = topic.databaseProvider
        res = self._simpleBlockingInvocation(databaseProvider, "_createTopic", fullServiceName=False, topic = topic)
        outputQueue.put(res)
        
    def _modifyTopic(self, inputQueue, outputQueue, topic):
        try:
            if topic.entityType is not "LiSS_TOPIC":
                response = Abstracts.Response("The given topic is invalid")
                outputQueue.put(response)
        except AttributeError:
            response = Abstracts.Response("The given topic is invalid")
            outputQueue.put(response)
            
        databaseProvider = topic.databaseProvider
        res = self._simpleBlockingInvocation(databaseProvider, "_createTopic", fullServiceName=False, topic = topic, force = True)
        outputQueue.put(res)
        
    """
    Utility method to assess the state of the given topic according to the existence of pending articles
    Post-conditions:
        Return the state "PENDING" if there are pending articles.
        Otherwise, return the state "READY"
    """
    def _getTopicState(self, topic):
        databaseProvider = topic.databaseProvider
        dbName = topic.topicID
        res = self._simpleBlockingInvocation(databaseProvider, "_readArticles", fullServiceName=False, dbName = dbName, collection = "pending", query = {})
        if len(res.returnValue) > 0:
            return "PENDING"
        else:
            return "READY"
        
    """
    This service moves the pending articles to the accepted and rejected list
    Args:
        articles - list of Article instances, storing decision of researcher
    Post-conditions:
        if the system is not in right state, cancel operation
        if input articles is invalid, cancel operation
        otherwise, return the result of underlying database operation.
    """
    def _submitDecision(self, inputQueue, outputQueue, topic, articles):
        # Check the state of the system. Return error message if the system is not in pending state
        topicState = self._getTopicState(topic)
        if topicState is "READY":
            response = Abstracts.Response("The system must be in PENDING state to invoke this operation.")
            outputQueue.put(response)
            return
        
        # Validate the given list of articles
        if not isinstance(articles, list):
            response = Abstracts.Response("The articles parameter must be a list.")
            outputQueue.put(response)
        if len(articles) > 0:
            if not isinstance(articles[0], Abstracts.Article):
                response = Abstracts.Response("Items in the list of articles must be instances of Article class")
                outputQueue.put(response)
        else:
            response = Abstracts.Response("The list of article is empty. Operation cancelled.")
            outputQueue.put(response)
            
        # Invoke the service of database to accept articles
        articleIDs = [article.articleID for article in articles]
        dbName = topic.topicID
        response = self._simpleBlockingInvocation(topic.databaseProvider, "_acceptArticles", fullServiceName=False, dbName = dbName, articleIDs = articleIDs)
        outputQueue.put(response)
        
    """
    This service performs primary query process.
    Args:
        topic - an instance of Topic class
    Post-conditions:
        if the system is not in READY state, cancel
        if a primary searcher has error, skip and work with the next one
        otherwise, store newly found articles in pending collection and return a success message
    """
    def _primaryQuery(self, inputQueue, outputQueue, topic):
        # Check the state of the system. Return error message if the topic is in pending
        topicState = self._getTopicState(topic)
        if topicState is "PENDING":
            response = Abstracts.Response("The system must be in READY state to invoke this operation. Please resolve pending articles first.")
            outputQueue.put(response)
            return
        
        databaseProvider = topic.databaseProvider
        # Invoke primary search service to get articles
        allFoundArticles = {}
        for i in range(len(topic.primarySearchProviders)):
            primarySearcher = str(topic.primarySearchProviders[0]["name"])
            query = topic.primarySearchProviders[0]["query"]
            # Iteratively retrieve articles from repository until everything is gathered
            queue = self._simpleBlockingInvocation(primarySearcher, "_primarySearch", getQueue = True, fullServiceName=False, query = query)
            finishPrimarySearch = False
            error = False
            foundArticles = []
            while not finishPrimarySearch:
                articles = []
                res = queue.get(timeout = 60)
                if res.isSuccess:
                    if res.isLastResponse:
                        response = Abstracts.Response("Primary Searcher %s responded with %d articles. No more articles." % (primarySearcher, len(res.returnValue)),
                                                  isSuccess=True, isLastResponse=False)
                        finishPrimarySearch = True
                    else:
                        response = Abstracts.Response("Primary Searcher %s responded with %d articles. More articles remain." % (primarySearcher, len(res.returnValue)),
                                                  isSuccess=True, isLastResponse=False)
                    outputQueue.put(response)
                    articles = res.returnValue
                    foundArticles.extend(articles)
                else:
                    response = Abstracts.Response("Error in primary search on %s. Skipping to next primary search." % primarySearcher)
                    outputQueue.put(response)
                    break
                    
            # Skip to next primary searcher if the current one has error
            if error:
                continue
            
#            print("In searcher %s" % primarySearcher)
#            print("Found articles: %s" % [(x.articleID, x.articleName) for x in foundArticles])
            
            # Check for duplication with the accepted and rejected articles
            response = Abstracts.Response("Finished primary search with %s. Start checking for duplication..." % primarySearcher, isLastResponse=False)
            outputQueue.put(response)
            res = self._simpleBlockingInvocation(databaseProvider, "_findPossibleDuplicates", fullServiceName=False, dbName = topic.topicID, collection = "accepted", articles = foundArticles)
            foundArticles = res.returnValue
            if res.isSuccess:
                response = Abstracts.Response("In accepted collection: %s" % res.responseMessage, isLastResponse=False)
                outputQueue.put(response)
            else:
                response = Abstracts.Response("No duplication in accepted articles", isLastResponse=False)
                outputQueue.put(response)
                
            res = self._simpleBlockingInvocation(databaseProvider, "_findPossibleDuplicates", fullServiceName=False, dbName = topic.topicID, collection = "rejected", articles = foundArticles)
            foundArticles = res.returnValue
            if res.isSuccess:
                response = Abstracts.Response("In rejected collection: %s" % res.responseMessage, isLastResponse=False)
                outputQueue.put(response)
            else:
                response = Abstracts.Response("No duplication in rejected articles", isLastResponse=False)
                outputQueue.put(response)
                
            print("After resolving duplications with accepted and rejected articles: %s" % 
                  [(x.articleID, x.articleName, x.duplicate) for x in foundArticles])
                
            # Resolve duplication among articles found by different primary search provider
            # Add articles found by this primary searcher into the set of all articles
            existingArticleNames = allFoundArticles.keys()
            for art in foundArticles:
                # Only add unique articles into the total list of found articles
                duplicate = False
                for artName in existingArticleNames:
                    if fuzz.token_set_ratio(art.articleName, artName) > 95:
                        duplicate = True
                        break
                if not duplicate:
                    allFoundArticles[art.articleName] = art
        
        response = Abstracts.Response("Finished primary query. %d articles found." % len(allFoundArticles.values()), isLastResponse=False)
        outputQueue.put(response)
        
        # Store primary search results in pending database and return to user
#        print([article.articleID for article in foundArticles])
#        print(len(foundArticles))
        res = self._simpleBlockingInvocation(databaseProvider, "_createArticles", fullServiceName=False, dbName = topic.topicID, collection = "pending", articles = allFoundArticles.values())
        if res.isSuccess:
            response = Abstracts.Response("Articles are stored in the pending collection", isSuccess=True)
            outputQueue.put(response)
        else:
            print(res.responseMessage)
            response = Abstracts.Response("Error during storing article to pending collection. Operation cancelled.")
            outputQueue.put(response)
            return
        
    """
    Probing the number of search result on each repository before performing primary search
    """
    def _probe(self, inputQueue, outputQueue, topic):
        # Invoke primary search service to get articles
        # For this mockup, only calls the first primary search
        for i in range(len(topic.primarySearchProviders)):
            primarySearcher = str(topic.primarySearchProviders[0]["name"])
            query = topic.primarySearchProviders[0]["query"]
            res = self._simpleBlockingInvocation(primarySearcher, "_probe", fullServiceName=False, query = query)
            res.isLastResponse = False
            outputQueue.put(res)
        response = Abstracts.Response("Finished probing.", isSuccess=True)
        outputQueue.put(response)
        
    """
    Performing snowballing search on accepted and unsnowballed articles
    Post-conditions:
        if the state is not READY, cancel
        else, update the status of snowballed articles and their referrers list
    """
    def _snowballingQuery(self, inputQueue, outputQueue, topic, pending = False):
        # Check the state of the system. Return error message if the topic is in pending
        topicState = self._getTopicState(topic)
        if topicState is "PENDING":
            response = Abstracts.Response("The system must be in READY state to invoke this operation. Please resolve pending articles first.")
            outputQueue.put(response)
            return
        
        databaseProvider = topic.databaseProvider   
        dbName = topic.topicID
        # Get snowballable articles
        res = self._simpleBlockingInvocation(databaseProvider, "_readArticles", fullServiceName=False, dbName = dbName, collection = "accepted", query = {"status" : "ACCEPTED"})
        articles = res.returnValue
        if len(articles) == 0:
            response = Abstracts.Response("No snowballable article detected. Cannot perform snowballing query.")
            outputQueue.put(response)
            return
        allReferences = {}
        # For each snowballing article
        for article in articles:
            # Extract references of each article
            queue = self._simpleBlockingInvocation(str(topic.snowballingSearchProviders[0]), "_snowballingSearch", fullServiceName=False, getQueue=True, article = article)
            while True:
                res = queue.get(timeout = 60)
                response = Abstracts.Response(res.responseMessage, isLastResponse=False)
                outputQueue.put(response)
                if res.isLastResponse:
                    # Unsuccessful snowballing query
                    if not res.isSuccess:
                        break
                    
                    # Successful snowballing query
                    # Merge the list of references of the current article to the lists of previous ones
                    references = res.returnValue
                    currentReferenceIDs = allReferences.keys()
                    print("References for article %s - %s" % (article.articleID, article.articleName))
                    print("References: %s" % str([(x.articleID, x.articleName) for x in references]))
                    for reference in references:
                        # If this reference is already cited by other articles
                        if reference.articleID in currentReferenceIDs:
                            allReferences[reference.articleID].addReferers({article.articleID : 1})
                        else:
                            allReferences[reference.articleID] = reference
                    break
            article.status = "SNOWBALLED"
#            print("Finish snowballing article %s - %s" % (article.articleID, article.articleName))
#            print("Found following references:")
#            print([(x.articleID, x.articleName) for x in references])
#            print("Current list of allReferences")
#            print([x for x in allReferences.items()])
#            print([x.referers for x in allReferences.values()])
        # Update the status of snowballed articles
        
        
        # Find duplicates of newly detected articles from accepted list
        # The duplicated reference is removed from the list, and its referrers are added to the existing accepted article
        response = Abstracts.Response("Finished snowballing search. Start checking for duplication...", isLastResponse=False)
        outputQueue.put(response)
        
        res = self._simpleBlockingInvocation(databaseProvider, "_findPossibleDuplicates", fullServiceName=False, dbName = topic.topicID, collection = "accepted", articles = allReferences.values())
        if res.isSuccess:
            response = Abstracts.Response("In accepted collection: %s" % res.responseMessage, isLastResponse=False)
            outputQueue.put(response)
            for art in res.returnValue:
                # Skip articles without duplication
                if not art.duplicate:
                    continue
                else:
                    # Replace the added article with the existing article
                    for article in articles:
                        if article.articleID == art.articleID:
                            article.addReferers(allReferences[article.articleID].referers)
                            del allReferences[article.articleID]
                            break                    
        else:
            response = Abstracts.Response("No duplication in accepted articles", isLastResponse=False)
            outputQueue.put(response)

        
        res = self._simpleBlockingInvocation(databaseProvider, "_findPossibleDuplicates", fullServiceName=False, dbName = topic.topicID, collection = "rejected", articles = allReferences.values())
        if res.isSuccess:
            response = Abstracts.Response("In rejected collection: %s" % res.responseMessage, isLastResponse=False)
            outputQueue.put(response)
            for art in res.returnValue:
                # Skip articles without duplication
                if not art.duplicate:
                    continue
                else:
                    # delete articles with duplications
                    del allReferences[art.articleID]
        else:
            response = Abstracts.Response("No duplication in rejected articles", isLastResponse=False)
            outputQueue.put(response)
        
        # Update the list of accepted article to reflect their changed state and new reference relationships
        res =self._simpleBlockingInvocation(databaseProvider, "_createArticles", fullServiceName=False, dbName = topic.topicID, collection = "accepted", articles = articles, update = True)
        if res.isSuccess:
            response = Abstracts.Response("Updated accepted articles", isSuccess=True, isLastResponse=False)
            outputQueue.put(response)
        else:
            response = Abstracts.Response("Error during updating accepted collection. Operation cancelled.")
            outputQueue.put(response)
            return
        
#        print("Final list of references before sending to database:")
#        print([(x.articleID, x.articleName, x.referers) for x in allReferences.values()])
        res = self._simpleBlockingInvocation(databaseProvider, "_createArticles", fullServiceName=False, dbName = topic.topicID, collection = "pending", articles = allReferences.values())
        if res.isSuccess:
            response = Abstracts.Response("Articles are stored in the pending collection", isSuccess=True)
            outputQueue.put(response)
        else:
            response = Abstracts.Response("Error during storing article to pending collection. Operation cancelled.")
            outputQueue.put(response)
            return
        
        response = Abstracts.Response("Finish snowballing.", isSuccess=True, returnValue=allReferences)
        outputQueue.put(response)
        
    def _removePendingArticles(self, inputQueue, outputQueue, topic):
        dbName = topic.topicID
        databaseProvider = topic.databaseProvider
        res = self._simpleBlockingInvocation(databaseProvider, "_removePendingArticles", fullServiceName=False, dbName = dbName)
        outputQueue.put(res)
        return
        
        
if __name__ == "__main__":
    inputQueue = Queue.Queue()
    outputQueue = Queue.Queue()
    
    #Topic object
        
    # Load JSON
    topicJSON = None
    with(open("sampleTopic.json", "r")) as inFile:
        topicJSON = json.loads(inFile.read())
    
    topic = Abstracts.Topic(str(topicJSON["topicID"]), str(topicJSON["topicName"]), str(topicJSON["databaseProvider"]),
                            topicJSON["primarySearchProviders"], topicJSON["snowballingSearchProviders"])
    
    # Registry service
    registry = Abstracts.AbsRegistry(moduleParams = {}, dependencies = {}, registry = None)
    registry.start()
    
    # DB manager service
    # Generate test articles
    articles = []
    for i in range(5):
        articles.append(Abstracts.Article("a%d" % i, "Test Article %d" % i, {}, "http://repository.com"))
    articleIDs = [article.articleID for article in articles]
    
    connInfo = {"host" : "localhost","port" : 27017,
                "col" : {"PENDING" : "pending", "ACCEPTED" : "accepted", "REJECTED" : "rejected", "LOG" : "log", "TOPIC" : "topic"}}
    dbProvider = MongoDBProvider.MongoDBProvider(moduleParams = {"connInfo" : connInfo}, dependencies = [],
                                 registry = registry)
    dbProvider.start()
#    dbProvider.addRequest("MongoDB_findPossibleDuplicates", inputQueue, outputQueue, dbName = topic.topicID, collection = "pending", articles = articles)
    articles = dbProvider._simpleBlockingInvocation(topic.databaseProvider, "_readArticles", fullServiceName=False, dbName = topic.topicID, collection = "pending", query = {}).returnValue

    # ScopusPrimarySearcher
    apiKey = "d7b66ca60d321c1a420d6969e0ff2e6e"
    view = "COMPLETE"
    httpAccept = "application/json"
    baseURL = "http://api.elsevier.com/content/search/scopus"
    params = {"view" : view, "apiKey" : apiKey, "httpAccept" : httpAccept}
    moduleParams = {"baseURL" : baseURL}
    moduleParams.update(params)
    pSearcher = ScopusPrimarySearcher.ScopusPrimarySearcher(moduleParams = moduleParams, dependencies = [], registry = registry)
    pSearcher.start()
    
    # ScopusSnowballingSearcher
    apiKey = "d7b66ca60d321c1a420d6969e0ff2e6e"
    view = "REF"
    httpAccept = "application/json"
    baseURL = "http://api.elsevier.com/content/abstract/doi/"
    params = {"view" : view, "apiKey" : apiKey, "httpAccept" : httpAccept}
    moduleParams = {"baseURL" : baseURL}
    moduleParams.update(params)
    sSearcher = ScopusSnowballingSearcher.ScopusSnowballingSearcher(moduleParams = moduleParams, dependencies = [], registry = registry)  
    sSearcher.start()
    
    # LiSS
    LiSS = LiSSProvider(moduleParams = {}, dependencies = {}, registry = registry)
    LiSS.start()
#    LiSS.addRequest("LiSS_openTopic", inputQueue, outputQueue, topic = topic)
#    LiSS.addRequest("LiSS_modifyTopic", inputQueue, outputQueue, topic = topic)
#    LiSS.addRequest("LiSS_submitDecision", inputQueue, outputQueue, topic = topic, articles = articles)
#    LiSS.addRequest("LiSS_removePendingArticles", inputQueue, outputQueue, topic = topic)

#    dbProvider.addRequest("MongoDB_createTopic", inputQueue, outputQueue, topic = topic) 
    
    # Test invoking services
    LiSS.addRequest("LiSS_primaryQuery", inputQueue, outputQueue, topic = topic)
#    LiSS.addRequest("LiSS_probe", inputQueue, outputQueue, topic = topic)
#    sSearcher.addRequest("LiSS_SCOPUS_SSEARCHER_snowballingSearch", inputQueue, outputQueue, article = articles[0])
#    LiSS.addRequest("LiSS_snowballingQuery", inputQueue, outputQueue, topic = topic)    


    while True:
        res = outputQueue.get(timeout = 60)
        print(res.responseMessage)
        if res.isLastResponse:
            break

