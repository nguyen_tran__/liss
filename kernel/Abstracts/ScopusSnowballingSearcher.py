# -*- coding: utf-8 -*-
"""
Created on Mon May 29 12:25:16 2017

@author: Nguyen Tran
"""
# libraries for REST client
import requests
from requests.utils import quote
try:
    from urlparse import urlparse
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urlparse
    from urllib.parse import urljoin
import Abstracts
import Queue

class ScopusSnowballingSearcher(Abstracts.AbsSnowballingSearcherWeb):
    def __init__(self, **kwargs):
        kwargs["providerName"] = "LiSS_SCOPUS_SSEARCHER"
        super(ScopusSnowballingSearcher, self).__init__(**kwargs)
        self._addAttribute("view", self.moduleParams, "view")
        self._addAttribute("httpAccept", self.moduleParams, "httpAccept")

    def _snowballingSearch(self, inputQueue, outputQueue, article, maxIter = 3):
        # Get IDs of referencing articles
        doi = article.metadata["doi"]
        queryURL = urljoin(self.baseURL, doi)
        params = {"view" : self.view, "apiKey" : self.apiKey, "httpAccept" : self.httpAccept}
        r = requests.get(queryURL, params = params)
        jsonResponse = r.json()
        try:
            ids = self._parseSearchResult(jsonResponse)   
        except KeyError:
            response = Abstracts.Response("Cannot extract references of article %s" % article.articleName)
            outputQueue.put(response)
            return
        response = Abstracts.Response("Extract %d references from article %s" % (len(ids), article.articleName), isLastResponse=False)
        outputQueue.put(response)
        
        referencedArticles = []
        count = 1
        for articleID in ids:
            art = self._getArticleByScopusID(articleID)
            # Skip error articles
            if not art:
                continue
            art.addReferers({article.articleID : 1})
            response = Abstracts.Response("--Retrieved %d over %d references." % (count, len(ids)), isLastResponse=False)
            outputQueue.put(response)
            referencedArticles.append(art)
            count += 1
            if count == maxIter:
                break
            
        response = Abstracts.Response("Finish snowballing for article %s" % article.articleName, isSuccess=True, returnValue=referencedArticles)
        outputQueue.put(response)
    
    def _getArticleByScopusID(self, scopusID):
        baseURL = "http://api.elsevier.com/content/abstract/scopus_id/"
        queryURL = urljoin(baseURL, scopusID)
        params = {"view" : "META_ABS", "apiKey" : self.apiKey, "httpAccept" : self.httpAccept}
        r = requests.get(queryURL, params = params)
        jsonResponse = r.json()
        # Detect error status code
        statusCode = jsonResponse.get("service-error", {}).get("status", {}).get("statusCode", "")
        if statusCode is not "":
            return None
        articleID = jsonResponse.get("abstracts-retrieval-response", {}).get("coredata", {}).get("dc:identifier", "")
        articleName = jsonResponse.get("abstracts-retrieval-response", {}).get("coredata", {}).get("dc:title", "")
        date = jsonResponse.get("abstracts-retrieval-response", {}).get("coredata", {}).get("prism:coverDate", "")
        abstract = jsonResponse.get("abstracts-retrieval-response", {}).get("coredata", {}).get("dc:description", "")
        doi = jsonResponse.get("abstracts-retrieval-response", {}).get("coredata", {}).get("prism:doi", "")
        authors = jsonResponse.get("abstracts-retrieval-response", {}).get("authors", {}).get("author", [])
        authorsInfo = []
        for author in authors:
            authorID = author.get("@auid", "")
            authorName = author.get("ce:indexed-name", "")
            authorsInfo.append({"authorID" : authorID, "authorName" : authorName})
        kws = jsonResponse.get("abstracts-retrieval-response", {}).get("authkeywords", {}).get("author-keyword", [])
        keywords = ""
        for kw in kws:
            keywords = "%s | %s" % (keywords, kw["$"])
        metadata = {"abstract" : abstract, "authors" : authorsInfo, "keywords" : keywords, "date" : date, "doi" : doi}
        article = Abstracts.Article(str(articleID), str(articleName), metadata, source = baseURL)
        return article
        
        
    
    def _parseSearchResult(self, result):
        references = result["abstracts-retrieval-response"]["references"]["reference"]
        ids = [reference.get("scopus-id", "") for reference in references]
        return ids