# -*- coding: utf-8 -*-
"""
Created on Wed May 24 12:42:22 2017

@author: gentr
"""

# libraries for REST client
import requests
from requests.utils import quote
try:
    from urlparse import urlparse
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urlparse
    from urllib.parse import urljoin
import Abstracts
import Queue

class ScopusPrimarySearcher(Abstracts.AbsPrimarySearcherWeb):
    def __init__(self, **kwargs):
        kwargs["providerName"] = "LiSS_SCOPUS_PSEARCHER"
        super(ScopusPrimarySearcher, self).__init__(**kwargs)
        self._addAttribute("view", self.moduleParams, "view")
        self._addAttribute("httpAccept", self.moduleParams, "httpAccept")       
    
    def _primarySearch(self, inputQueue, outputQueue, query, maxIter = 2, count = 1):
        params = {"view" : self.view, "apiKey" : self.apiKey, "httpAccept" : self.httpAccept, "query" : query, "count" : count}
        count = 0
        r = requests.get(self.baseURL, params = params)
        while True:
            jsonResponse = r.json()
            articles = self._parseSearchResult(jsonResponse)
            count += 1
            # Get next URL
            try:
                nextURL = jsonResponse["search-results"]["link"][3]["@href"]
                if count == maxIter:
                    raise RuntimeError()
            except:
                # Cannot get next URL, so assume that no more results remain
                response = Abstracts.Response("Found %d documents from Scopus. Finished." % len(articles), returnValue = articles, isSuccess = True)
                outputQueue.put(response)
                return
            
            # Can get next URL, so there are more results
            response = Abstracts.Response("Found %d documents from Scopus. More articles pending." % len(articles), returnValue = articles, isSuccess = True, isLastResponse=False)
            outputQueue.put(response)
            r = requests.get(nextURL)            
        
    """
    Check the number of articles matching the given keyword
    """
    def _probe(self, inputQueue, outputQueue, query):
        params = {"view" : self.view, "apiKey" : self.apiKey, "httpAccept" : self.httpAccept, "query" : query, "count" : 1}
        r = requests.get(self.baseURL, params = params)
        numResults = int(r.json().get("search-results", {}).get("opensearch:totalResults", -1))
        if numResults == -1:
            response = Abstracts.Response("Received invalid response from Scopus")
            outputQueue.put(response)
            raise RuntimeError("Received invalid result from Scopus")
        else:
            response = Abstracts.Response("Found %d documents from Scopus with the given query [%s]" % (numResults, str(query)), isSuccess = True)
            outputQueue.put(response)
        
    # Mockup search results, which is a list of article
    def _parseSearchResult(self, result):
        articles = []
        entries = result["search-results"]["entry"]
        for entry in entries:
            # Build metadata of the paper
            metadata = {}
            abstract = entry.get("dc:description", "")
            authors = entry.get("author", [])
            authorsInfo = []
            for author in authors:
                authorID = author.get("authid", "")
                authorName = author.get("authname", "")
                authorsInfo.append({"authorID" : authorID, "authorName" : authorName})
            keywords = entry.get("authkeywords", "")
            date = entry.get("coverDate", "")
            doi = entry.get("prism:doi", "")
            metadata = {"abstract" : abstract, "authors" : authorsInfo, "keywords" : keywords, "date" : date, "doi" : doi}
            articles.append(Abstracts.Article(str(entry["dc:identifier"]), str(entry["dc:title"]), metadata, self.baseURL))
        return articles
    
    def __parseSearchResult(self, result):
        return result.json()
    
if __name__ == "__main__":
    apiKey = "d7b66ca60d321c1a420d6969e0ff2e6e"
#    view = "REF"
    view = "COMPLETE"
    httpAccept = "application/json"
#    baseURL = "http://api.elsevier.com/content/abstract/doi/"
    baseURL= "http://api.elsevier.com/content/search/scopus"
    doi = "10.1109/JIOT.2014.2306328"
    queryURL = urljoin(baseURL, doi)
    params = {"view" : view, "apiKey" : apiKey, "httpAccept" : httpAccept}
    moduleParams = {"baseURL" : baseURL}
    moduleParams.update(params)
#    r = requests.get(queryURL, params = params)
#    print(type(r.json()))
    
    pSearcher = ScopusPrimarySearcher(moduleParams = moduleParams, dependencies = [], registry = None)
    pSearcher.start()
    inputQueue = Queue.Queue()
    outputQueue = Queue.Queue()
    pSearcher.addRequest("LiSS_SCOPUS_PSEARCHER_primarySearch", inputQueue, outputQueue, query = doi)
    print(outputQueue.get().returnValue)
    pSearcher.stop()
    
    