# -*- coding: utf-8 -*-
"""
Created on Tue May 23 12:19:44 2017

@author: Nguyen Tran

This module contains all abstract classes for developing providers in the service
"""
import threading
import Queue
import time

"""
=======================================================
Abstract Provider - Ancestor of all other providers
=======================================================
"""
class AbsProvider(object):
    def __init__(self, **kwargs):
        self._addAttribute("providerType", kwargs, "providerType")
        self._addAttribute("providerName", kwargs, "providerName")
        self._addAttribute("moduleParams", kwargs, "moduleParams")
        self._addAttribute("dependencies", kwargs, "dependencies")
        self._addAttribute("registry", kwargs, "registry")
        self.services = {}
        
        # Thread-related variables
        self.requests = Queue.Queue()
        self.isRunning = False
        self.t = threading.Thread(target = self._listen)
    
    """
    Start the listening thread of this provider
    """
    def start(self):
        self.isRunning = True
        self.t.start()
    
    """
    Stop the listening thread of this provider
    """
    def stop(self):
        self.isRunning = False
    
    """
    Listen to the request queue until explicitly stopped by the stop() method
    """
    def _listen(self):
        while self.isRunning:
            req = self.requests.get()
            self._invokeOwnService(req)
    
    """
    Public method for adding a request to the service.
    Args:
        serv - name of the service to be invoked (string)
        inputQueue - queue for sending information to the service
        outputQueue - queue for returning information from the service
        kwargs - optional set of keyword arguments
    Post-conditions:
        If args are valid, a request object is added to the queue to be processed
        by the service.
        Else, the request is dropped
    """
    def addRequest(self, serv, inputQueue, outputQueue, **kwargs):
        try:
            req = Request(serv, inputQueue, outputQueue, **kwargs)
            self.requests.put(req)
        except TypeError:
            print("Received an invalid request. (TypeError)")
            pass
        except AttributeError:
            print("Received an invalid request.(AttributeError)")
            pass
        
    """
    Extract value from an array or dictionary and assign to the object
    as an attribute
    Args:
        attrName - name of the attribute to be added (string)
        fromParams - a list or dictionary containing the value to be extracted
        index - index of the extracting value
        moduleType - unused
    """
    def _addAttribute(self, attrName, fromParams, index, moduleType = None):
        errMes = "Cannot find %s in %s. It must be supplied at index %s" % (attrName, fromParams, index)
        try:
            attr = fromParams[index]
            if (moduleType is not None) and (moduleType != getattr(attr, "type")):
                print(attr, "type")
                raise ReferenceError(errMes)
        except:
            raise ReferenceError(errMes)
        setattr(self, attrName, attr)
        
    """
    Bind a method to the list of services of this provider. Service name is 
    determined automatically based on the name of provider and the method
    Args:
        methodName - name of the method to be added (string)
    Pre-conditions:
        None
    Post-conditions:
        the specified method is added to the services dictionary
    """
    def _bindMethodToService(self, methodName):
        # Let exception crash the initialization process 
        method = getattr(self, methodName)
        self.services[self.providerName + methodName] = method

    """
    Publish the list of services provided by this provider to the registry
    """
    def _publishServices(self):
        inputQueue = Queue.Queue()
        outputQueue = Queue.Queue()
        self._invokeOtherService(self.registry, "LiSS_REGISTRY_addService",
                                 inputQueue, outputQueue, provider = self)
        res = outputQueue.get()
        return
        
    """
    Invoke a service of this provider
    Args:
        req - a Request object
    Pre-conditions:
        req is a valid Request object, provided by the kernel
    Post-conditions:
        if the service can be found, it is invoked and executed on a separated thread
        else, an error message stating that message is sent
    """    
    def _invokeOwnService(self, req):
        # Return the error message instead of exception because this function
        # is invoked in runtime when interacting with consumers
        serviceName = req.serviceName
        inputQueue = req.inputQueue
        outputQueue = req.outputQueue
        kwargs = req.kwargs
        method = None
        try:
            method = self.services[serviceName]
        except KeyError:
            response = Response("Cannot invoke the service " + serviceName + ". The name might be incorrect")
            try:
                outputQueue.put(response)
            except:
                return None
            return None
        t = threading.Thread(target = method, args = [inputQueue, outputQueue], kwargs=kwargs)
        t.start()
        return t

    """
    Put a request on the queue of other provider for a service. Doesn't perform any validation
    Args:
        provider - a provider instance that holds the required service
        serviceName - name of the service (string)
        inputQueue - queue for sending information to the service
        outputQueue - queue for returning information from the service
        kwargs - optional set of keyword arguments
    """
    def _invokeOtherService(self, prov, serv, inputQueue, outputQueue, **kwargs):
        prov.addRequest(serv, inputQueue, outputQueue, **kwargs)
        
    """
    Query the registry for provider of a given service name. 
    Args:
        serv - name of the service (string)
    Post-conditions:
        If service is found, return the reference to its provider
        else, raise an index error
    """
    def _getProviderByServiceName(self, serv):
        inputQueue = Queue.Queue()
        outputQueue = Queue.Queue()
        self._invokeOtherService(self.registry, "LiSS_REGISTRY_findProvider", 
                                 inputQueue, outputQueue, serviceName = serv)
        res = outputQueue.get()
        if not res.isSuccess:
            raise IndexError("Cannot find any provider for service " + serv)
        else:
            return res.returnValue
            
    """
    Invoking a service on a provider. A provider instance or a name can be supplied.
    """
    def _simpleBlockingInvocation(self, invokedProvider, invokedName, fullServiceName = True, timeout = 60, getQueue = False, **kwargs):
        inputQueue = Queue.Queue()
        outputQueue = Queue.Queue()
        # Initialize the name of the service
        if not fullServiceName:
            invokedName = "%s%s" % (invokedProvider, invokedName)
        # If a string is given, find the provider first
        if (not isinstance(invokedProvider, AbsProvider)):
            if isinstance(invokedProvider, str):
                invokedProvider = self._getProviderByServiceName(invokedName)
            else:
                response = Response("The given invoked provider is invalid. It must be either a provider instance or a string")
                outputQueue.put(response)
                return
       
        self._invokeOtherService(invokedProvider, invokedName, inputQueue, outputQueue, **kwargs)
        if getQueue:
            return outputQueue
        else:
            try:
                res = outputQueue.get(timeout = timeout)
                return res
            except Queue.Empty:
                raise RuntimeError("Service failed to response during the time limit.")
            


"""
=======================================================
Abstract Registry
=======================================================
"""
class AbsRegistry(AbsProvider):
    def __init__(self, **kwargs):
        kwargs["providerType"] = "LiSS_REGISTRY"
        kwargs["providerName"] = "LiSS_REGISTRY"
        super(AbsRegistry, self).__init__(**kwargs)
        self.registeredServices = {}
        self._bindMethodToService("_addService")
        self._bindMethodToService("_findProvider")
    
    """
    Add services of a given provider to the registry
    Args:
        provider - a provider instance that holds services to be added
        inputQueue - queue for sending information to the service
        outputQueue - queue for returning information from the service 
    Post-conditions:
        All unique services are added to the registry. In case of duplication, the 
        new service is rejected.
    """
    def _addService(self, inputQueue, outputQueue, provider):
        # Extract service names
        try:
            serviceNames = provider.services.keys()
        except:
            response = Response("Cannot extract the list of services from the given provider")
            outputQueue.put(response)
            return
        # Update the list of registered services. Reject duplicated services
        rejectedServices = []
        for serviceName in serviceNames:
            if not(self.services.get(serviceName) is None):
                rejectedServices.append(serviceName)
            else:
                self.registeredServices[serviceName] = provider
        # report results
        if len(rejectedServices) == len(serviceNames):
            response = Response("Cannot add services. They already exist in the library")
        elif len(rejectedServices) > 0:
            response = Response("Added services. Some duplicates are rejected: %s" % rejectedServices, isSuccess=True)
        else:
            response = Response("Added all services." % rejectedServices, isSuccess=True)
        outputQueue.put(response)
        return
    
    """
    Find the provider of the given service name
    Args:
        inputQueue - queue for sending information to the service
        outputQueue - queue for returning information from the service 
        serviceName - name of the service to be retrieved (string)
    Post-conditions:
        If the service is already registered, the provider is returned via the outputQueue
        else, return the error message. Content of the message depends on whether
        service is not found, or a program error occured during the look up.
    """
    def _findProvider(self, inputQueue, outputQueue, serviceName):
        try:
            provider = self.registeredServices.get(serviceName)
            if not(provider is None):
                response = Response("Service %s is found" % serviceName, isSuccess = True, returnValue = provider)
                outputQueue.put(response)
                return
            else:
                response = Response("Cannot find the service " + serviceName)
                outputQueue.put(response)
                return
        except:
            response = Response("An error occurred while looking up for service " + serviceName)
            outputQueue.put(response)
            return
        
"""
=======================================================
Abstract Primary Query
=======================================================
"""
class AbsPrimarySearcher(AbsProvider):
    def __init__(self, **kwargs):
        kwargs["providerType"] = "LiSS_PSEARCHER"
        super(AbsPrimarySearcher, self).__init__(**kwargs)
        self._bindMethodToService("_primarySearch")
        self._bindMethodToService("_probe")
        self._publishServices()

    """
    Primary search service invoke the given query against a predefined
    repository. Its implementation varies according to the provider.
    """
    def _primarySearch(self, inputQueue, outputQueue, query):
        raise NotImplementedError("Primary search service of this provider is not implemented")
    
    """
    Probe service check the number of search results generated from the given query
    Its implementation varies according to the provider
    """
    def _probe(self, inputQueue, outputQueue, query):
        raise NotImplementedError("Probe service of this provider is not implemented")
        
    """
    Parse search result method translate the record collected from the repostiory
    into the article object defined by the kernel. The actual parsing process
    is implemented by the concrete provider.
    """
    def _parseSearchResult(self, result):
        raise NotImplementedError("Parsing search result method of this provider is not implemented")
    
class AbsPrimarySearcherWeb(AbsPrimarySearcher):
    def __init__(self, **kwargs):
        super(AbsPrimarySearcherWeb, self).__init__(**kwargs)
        self._addAttribute("baseURL", self.moduleParams, "baseURL")
        self._addAttribute("apiKey", self.moduleParams, "apiKey")
    
"""
=======================================================
Abstract Snowballing Searcher
=======================================================
"""
class AbsSnowballingSearcher(AbsProvider):
    def __init__(self, **kwargs):
        kwargs["providerType"] = "LiSS_SSEARCHER"
        super(AbsSnowballingSearcher, self).__init__(**kwargs)
        self._bindMethodToService("_snowballingSearch")
        self._publishServices()

    """
    Produce a list of articles cited by the single given article
    """
    def _snowballingSearch(self, inputQueue, outputQueue, article):
        raise NotImplementedError("Snowballing search service of this provider is not implemented")
            
    """
    Parse search result method translate the record collected from the repostiory
    into the article object defined by the kernel. The actual parsing process
    is implemented by the concrete provider.
    """
    def _parseSearchResult(self, result):
        raise NotImplementedError("Parsing search result method of this provider is not implemented")
    
class AbsSnowballingSearcherWeb(AbsSnowballingSearcher):
    def __init__(self, **kwargs):
        super(AbsSnowballingSearcherWeb, self).__init__(**kwargs)
        self._addAttribute("baseURL", self.moduleParams, "baseURL")
        self._addAttribute("apiKey", self.moduleParams, "apiKey")
    
"""
=======================================================
Abstract Database Service Provider
=======================================================
"""
class AbsDatabase(AbsProvider):
    def __init__(self, **kwargs):
        kwargs["providerType"] = "LiSS_DB"
        super(AbsDatabase, self).__init__(**kwargs)
        self._addAttribute("connInfo", self.moduleParams, "connInfo")
        self._bindMethodToService("_createArticles")
        self._bindMethodToService("_createTopic")
        self._bindMethodToService("_removeArticles")
        self._bindMethodToService("_createLog")
        self._bindMethodToService("_readArticles")
        self._bindMethodToService("_findPossibleDuplicates")
        self._bindMethodToService("_readLogs")
        self._bindMethodToService("_readTopic")
        self._bindMethodToService("_acceptArticles")
        self._bindMethodToService("_removePendingArticles")
        
    
    """
    Connect to the database according to the given connInfo parameters.
    This method might also create indexes if they are not available
    """
    def _connect(self):
        raise NotImplementedError("_connect service of this database provider is not implemented")
    
    """
    Add a set of articles to one of the collection
    """
    def _createArticles(self, inputQueue, outputQueue, dbName, collection, article):
        raise NotImplementedError("_createArticles service of this database provider is not implemented")
    
    """
    Remove a set of articles from the given collection, according to the given query
    """
    def _removeArticles(self, inputQueue, outputQueue, dbName, collection, query):
        raise NotImplementedError("_removeArticles service of this database provider is not implemented")
    
    """
    Add a log to the database
    """
    def _createLog(self, inputQueue, outputQueue, dbName, logMessage):
        raise NotImplementedError("_createLog service of this database provider is not implemented")
    
    """
    Retrieve articles of a collection from database
    """
    def _readArticles(self, inputQueue, outputQueue, dbName, collection, query):
        raise NotImplementedError("_readArticles service of this database provider is not implemented")
    
    """
    Find existing articles in the collection that is likely a duplicate of the given article name
    """
    def _findPossibleDuplicates(self, inputQueue, outputQueue, dbName, collection, articleName):
        raise NotImplementedError("_findPossibleDuplicates service of this database provider is not implemented")
    
    """
    Retrieve logs from the database
    """
    def _readLogs(self, inputQueue, outputQueue, dbName, query):
        raise NotImplementedError("_readLogs service of this database provider is not implemented")
    
    """
    Retrieve logs from the database
    """
    def _readTopic(self, inputQueue, outputQueue, dbName, query):
        raise NotImplementedError("_readLogs service of this database provider is not implemented")
    
    """
    Move a set of articles from pending to accepted collection. Remaining ones are moved to rejected list
    """
    def _acceptArticles(self, inputQueue, outputQueue, dbName, articleList):
        raise NotImplementedError("_acceptArticles service of this database provider is not implemented")
        
    """
    Remove all pending articles
    """
    def _removePendingArticles(self, inputQueue, outputQueue, dbName):
        raise NotImplementedError("_removePendingArticles service of this database provider is not implemented")

"""
=======================================================
LiSS Provider
=======================================================
"""
"""
LiSS provider is stateless. The state of the application is calculated directly during runtime
from the data stored in the database
"""
class AbsLiSSProvider(AbsProvider):
    def __init__(self, **kwargs):
        kwargs["providerType"] = "LiSS"
        super(AbsLiSSProvider, self).__init__(**kwargs)
        self._bindMethodToService("_openTopic")
        self._bindMethodToService("_closeTopic")
        self._bindMethodToService("_modifyTopic")
        self._bindMethodToService("_primaryQuery")
        self._bindMethodToService("_probe")
        self._bindMethodToService("_snowballingQuery")
        self._bindMethodToService("_statusReport")
        self._bindMethodToService("_submitDecision")
        self._bindMethodToService("_requestDocCollection")
        self._bindMethodToService("_modifyDocCollection")
        self._bindMethodToService("_removePendingArticles")
    
    """
    Create or open an existing topic from database
    Args:
        topic - an instance of the Topic class, holding the information of the literature review topic
    """
    def _openTopic(self, inputQueue, outputQueue, topic):
        raise NotImplementedError("_openTopic service of this LiSS service provider is not implemented")
    
    def _closeTopic(self, inputQueue, outputQueue, topic):
        raise NotImplementedError("_closeTopic service of this LiSS service provider is not implemented")
    
    def _modifyTopic(self, inputQueue, outputQueue, modifiedTopic):
        raise NotImplementedError("_modifyTopic service of this LiSS service provider is not implemented")

    def _primaryQuery(self, inputQueue, outputQueue, topic):
        raise NotImplementedError("_primaryQuery service of this LiSS service provider is not implemented")
        
    def _snowballingQuery(self, inputQueue, outputQueue, topic):
        raise NotImplementedError("_snowballingQuery service of this LiSS service provider is not implemented")
        
    def _statusReport(self, inputQueue, outputQueue, topic):
        raise NotImplementedError("_statusReport service of this LiSS service provider is not implemented")
        
    """
    Submit decisions regarding articles in the pending list.
    Args:
        topic - an instance of the Topic class, holding the information of the literature review topic
        articles - a list of instances of Article class
    """
    def _submitDecision(self, inputQueue, outputQueue, topic, articles):
        raise NotImplementedError("_primaryQuery service of this LiSS service provider is not implemented")

    """
    Request the list of documents from a collection of the topic
    Args:
        topic - an instance of the Topic class, holding the information of the literature review topic
        collection - the name of the collection to be retrieved (string)
    """
    def _requestDocCollection(self, inputQueue, outputQueue, topic, collection):
        raise NotImplementedError("_requestDocCollection service of this LiSS service provider is not implemented")

    def _modifyDocCollection(self, inputQueue, outputQueue, topic, collection):
        raise NotImplementedError("_modifyDocCollection service of this LiSS service provider is not implemented")
        
    def _removePendingArticles(self, inputQueue, outputQueue, topic):
        raise NotImplementedError("_removePendingArticles service of this LiSS service provider is not implemented")

"""
=======================================================
Utility entities
=======================================================
"""
class Entity(object):
    def __init__(self, entityType):
        self._assignAttr("entityType", str, entityType)
    
    def _assignAttr(self, attrName, attrType, attrValue):
        self._validate(attrValue, attrType)
        setattr(self, attrName, attrValue)
    
    def _validate(self, attr, attrType):
        if not isinstance(attr, attrType):
            raise TypeError("Attribute %s must be of type %s. %s is given" % (attr, attrType, type(attr)))
        else:
            return
        
    def __iter__(self):
        ls = {}
        for key in self.__dict__.keys():
            ls[key] = self.__dict__[key]
        return ls

"""
This class represents a request object. It contains checks to ensure that
parameters are syntactically valid.
Args:
    serviceName - name of the service (string)
    inputQueue - queue for sending information to the service
    outputQueue - queue for returning information from the service
    kwargs - optional set of keyword arguments
Post-conditions:
    if args are syntactically valid, a request object is created
    else, an exception is raised
"""
class Request(Entity):
    def __init__(self, requestServiceName, inputQueue, outputQueue, **kwargs):
        super(Request, self).__init__("LiSS_REQUEST")
        self._assignAttr("serviceName", str, requestServiceName)
        self._assignAttr("inputQueue", Queue.Queue, inputQueue)
        self._assignAttr("outputQueue", Queue.Queue, outputQueue)
        self._assignAttr("kwargs", dict, kwargs)

"""
This class represents an article retrieved from the repository. It contains checks
to ensure that parameters are syntactically valid.
Args:
Post-conditions:
    if args are valid, an article object is created
    otherwise, an exception is raised
"""
class Article(Entity):
    def __init__(self, articleID, articleName, metadata, source, status = "UNDECIDED", referers = None, duplicate = None):
        super(Article, self).__init__("LiSS_ARTICLE")
        self._assignAttr("articleID", str, articleID)
        self._assignAttr("articleName", str, articleName)
        self._assignAttr("metadata", dict, metadata)
        self._assignAttr("source", str, source)
        self._assignAttr("status", str, status)
        if not referers:
            self.referers = {}
        else:
            self._assignAttr("referers", dict, referers)
        if not duplicate:
            self.duplicate = None
        else:
            self._assignAttr("duplicate", Article, duplicate)
        
        
    def accept(self):
        self.status = "ACCEPTED"
        
    def reject(self):
        self.status = "REJECTED"
        
    def snowball(self):
        self.status= "SNOWBALLED"
        
    def addReferers(self, referers):
#        print("Called add referer in %s - %s for %s" % (self.articleID, self.articleName, referers))
#        print("Before adding referer: %s" % self.referers)
        self._validate(referers, dict)
        for referer in referers.keys():
            citeCount = self.referers.get(referer, 0)
            if citeCount == 0:
                # If referer does not exist, add it to the list
                self.referers[referer] = 1
            else:
                # Otherwise, increase the count
                self.referers[referer] = citeCount + referers[referer]
#        print("After adding referer: %s" % self.referers)
        
    def addDuplicate(self, duplicate):
#        print("Called add referer in %s - %s for %s" % (self.articleID, self.articleName, duplicate.articleID))
#        print("Before: %s" % self.duplicate)
        self._validate(duplicate, Article)
        self.duplicate = duplicate
#        print("After: %s" % self.duplicate)
        

"""
This class represent the response that are passed between services in LiSS.
"""
    
class Response(Entity):
    def __init__(self, responseMessage, isSuccess = False, returnValue = None, isLastResponse = True):
        super(Response, self).__init__("LiSS_RESPONSE")
        self._assignAttr("responseMessage", str, responseMessage)
        self._assignAttr("isSuccess", bool, isSuccess)
        self.returnValue = returnValue
        self._assignAttr("isLastResponse", bool, isLastResponse)
        
"""
This class represent a literature review topic that is passed to an LiSS instance
Args:
    topicID - user-assigned ID for this research topic (string)
    topicName - user-assigned name of this research topic (string)
    query - query string utilized by this research topic(string)
    databaseService - the name of database service utilized by this topic (string)
    primarySearchServices - the list of primary search services utilized by this topic (list)
    snowballingSearchServices - the list of snowballing search services utilized by this topic (list)
"""
class Topic(Entity):
    def __init__(self, topicID, topicName, databaseProvider, 
                 primarySearchProviders, snowballingSearchProviders):
        super(Topic, self).__init__("LiSS_TOPIC")
        self._assignAttr("topicID", str, topicID)
        self._assignAttr("topicName", str, topicName)
        self._assignAttr("databaseProvider", str, databaseProvider)
        # Extra validation to ensure that content of the list of primary search providers is valid
        self._assignAttr("primarySearchProviders", list, primarySearchProviders)
        try:
            self._validate(primarySearchProviders[0], dict)
        except IndexError:
            raise IndexError("At least one primary search provider must be specified")
        # Extra validation to ensure that content of the list of snowballing search providers is valid
        self._assignAttr("snowballingSearchProviders", list, snowballingSearchProviders)
        try:
            self._validate(str(snowballingSearchProviders[0]), str)
        except IndexError:
            pass
        
        

"""
=======================================================
=======================================================
""" 
if __name__ == "__main__":
    article1 = Article("art 1", "article 1", {}, "http://scopus.com")
    article2 = Article("art 2", "article 2", {}, "http://scopus.com")
    article3 = Article("art 3", "article 3", {}, "http://scopus.com")
    article4 = Article("art 4", "article 4", {}, "http://scopus.com")
    
    article1.addReferers({article3.articleID : 1})
    article2.addReferers({article4.articleID : 1})
#    article1.addDuplicate(article3)
#    article2.addDuplicate(article4)
#    article1.accept()
#    article2.accept()
    print(article1.__dict__)
#    print(Article.__dict__)
            