# -*- coding: utf-8 -*-
"""
Created on Tue May 23 14:44:04 2017

@author: gentr
"""

import threading
import time
import Queue

class Provider(object):
    def __init__(self):
        self.requests = Queue.Queue()
        self.t = threading.Thread(target = self._listen)
        self.t.start()
    
    def _listen(self):
        while True:
            req = self.requests.get()
            threadName = req[0]
            inputQueue = req[1]
            threading.Thread(target = self.service, args = [threadName, inputQueue]).start()
    
    def addRequest(self, threadName, inputQueue):
        req = [threadName, inputQueue]
        self.requests.put(req)
    
    def service(self, threadName, inputQueue):
        print("Printing from thread " + threadName)
        time.sleep(5)
        print("\nThread " + threadName + " finished")
        inputQueue.put("**Response from thread " + threadName)

if __name__ == "__main__":
    provider = Provider()
    for i in range(5):
        threadName = "Thread-" + str(i)
        inputQueue = Queue.Queue()
        provider.addRequest(threadName, inputQueue)
