# -*- coding: utf-8 -*-
"""
Created on Mon May 29 09:58:31 2017

@author: gentr
"""

import requests
from requests.utils import quote
import json    
import random    
try:
    from urlparse import urlparse
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urlparse
    from urllib.parse import urljoin
import Abstracts
import Queue

if __name__ == "__main__":
    url = "http://api.elsevier.com/content/search/scopus"
    apiKey = "d7b66ca60d321c1a420d6969e0ff2e6e"
    httpAccept = "application/json"
    query = "{Internet of Things}"
    params = {"apiKey" : apiKey, "httpAccept" : httpAccept, "query" : query}
    r = requests.get(url, params=params)
    r.status_code
