# -*- coding: utf-8 -*-
"""
Created on Thu May 25 10:17:23 2017

@author: gentr
"""
import Abstracts
import pymongo
from pymongo import MongoClient
import Queue
import time
import datetime
from fuzzywuzzy import fuzz

class MongoDBProvider(Abstracts.AbsDatabase):
    def __init__(self, **kwargs):
        kwargs["providerName"] = "MongoDB"
        super(MongoDBProvider, self).__init__(**kwargs)
        self._addAttribute("host", self.connInfo, "host")
        self._addAttribute("port", self.connInfo, "port")
        self._addAttribute("colPending", self.connInfo["col"], "PENDING")
        self._addAttribute("colAccepted", self.connInfo["col"], "ACCEPTED")
        self._addAttribute("colRejected", self.connInfo["col"], "REJECTED")
        self._addAttribute("colLog", self.connInfo["col"], "LOG")
        self._addAttribute("colTopic", self.connInfo["col"], "TOPIC")
        self._publishServices()
        
    def _connect(self, dbName):
        def _createIndex(client, dbName, colName, field):
            db = client[dbName]
            if not(colName in db.collection_names(False)):
                col = db[colName]
                col.create_index([(str(field),1)], unique = True)
            return
        
        connAddr = "%s:%s" % (self.connInfo["host"], self.connInfo["port"])
#        print(connAddr)
        try:
            client = MongoClient(connAddr)
            _createIndex(client, dbName, self.colPending, "articleID")
            _createIndex(client, dbName, self.colAccepted, "articleID")
            _createIndex(client, dbName, self.colRejected, "articleID")
            return client
        except:
            raise RuntimeError("Cannot connect to MongoDB")
    
    def _getDBCol(self, dbName, colName):
        client = self._connect(dbName)
        db = client[dbName]
        col = db[colName]
        return db, col
        
    def _readFromDB(self, dbName, collection, query):
        db, col = self._getDBCol(dbName, collection)
        results = []
        cursor = None
        # Treat topic collection differently, due to the need of sorting
        if collection is self.colTopic:
            cursor = col.find(query).sort("creationTime", pymongo.DESCENDING)
        else:
            cursor = col.find(query)
        for item in cursor:
            del item["_id"]
            if collection is self.colPending or collection is self.colAccepted or collection is self.colRejected:
                article = Abstracts.Article(str(item["articleID"]), str(item["articleName"]), 
                                            item["metadata"], str(item["source"]), 
                                            str(item["status"]), item["referers"])
                results.append(article)
            elif collection is self.colLog:
                results.append(item)
            elif collection is self.colTopic:
                return item
        return results
    
    def _readFromDBCursor(self, dbName, collection, query):
        db, col = self._getDBCol(dbName, collection)
        return col.find(query)
    
    """
    Get article from the collection that has the name closest to the given article name.
    Args:
        collection - name of the collection (string)
        articleName - name of the article to find (string)
        threshold - the fuzzy matching score that is considered a match
    Pre-conditions:
        collection is valid
    Post-conditions:
        if article can be found, return it. Else, return None
    """
    def _readFromDBFuzzy(self, dbName, collection, articleName, threshold = 95):
        db, col = self._getDBCol(dbName, collection)
        for item in col.find({}):
            if fuzz.token_set_ratio(item["articleName"], articleName) >= threshold:
                article = Abstracts.Article(str(item["articleID"]), str(item["articleName"]), 
                                        item["metadata"], str(item["source"]), 
                                        str(item["status"]), item["referers"])
                return article
        return None
    
    """
    Bulk insert of update.
    Pre-conditions:
        data is a list of dictionary
    """
    def _bulkInsert(self, dbName, collection, data, update = False):
        db, col = self._getDBCol(dbName, collection)
        # Perform bulk insert if the current request is for inserting, not updating
        if not update:
            return col.insert_many(data, ordered = False)
        else:
            for art in data:
                col.replace_one({"articleID" : art["articleID"]}, art)
            return "Updated %d documents in %s collection: %s" % (len(data), collection, [x["articleID"] for x in data])
    
    def _remove(self, dbName, collection, query):
        db, col = self._getDBCol(dbName, collection)
        return col.delete_many(query)
    
    def _removeArticles(self, inputQueue, outputQueue, dbName, collection, articleIDs):
        query = {"articleID" : {"$in" : articleIDs}}
        # Check the type of collection to insert
        colName = ""
        if collection is "pending":
            colName = self.colPending
        elif collection is "accepted" :
            colName = self.colAccepted
        elif collection is "rejected":
            colName = self.colRejected
        else:
            response = Abstracts.Response("The collection must be one of [pending, accepted, rejected]")
            outputQueue.put(response)
            
        # Remove the articles
        result = None
        try:
            result = self._remove(dbName, colName, query)
            response = Abstracts.Response("Removed %d articles from %s collection" % (result.deleted_count, colName), isSuccess=True)
            outputQueue.put(response)
            if colName is not "pending":
                self._simpleBlockingInvocation(self, "MongoDB_createLog", dbName = dbName, logMessage = "Removed %d articles from %s collection." % (result.deleted_count, colName))  
        except TypeError:
            response = Abstracts.Response("TypeError: The given article list is invalid to be remove from MongoDB")
            outputQueue.put(response)
            return
    
    def _createArticles(self, inputQueue, outputQueue, dbName, collection, articles, update = False):
        # Check the type of collection to insert
        colName = ""
        if collection is "pending":
            colName = self.colPending
        elif collection is "accepted" :
            colName = self.colAccepted
        elif collection is "rejected":
            colName = self.colRejected
        else:
            response = Abstracts.Response("The collection must be one of [pending, accepted, rejected]")
            outputQueue.put(response)
            
        # If the duplicate is an article object, return it into a dictionary
        for a in articles:
            dup = getattr(a, "duplicate", None)
            if not dup:
                continue
            else:
                if isinstance(dup, Abstracts.Article):
                    a.duplicate = {"articleID" : dup.articleID, "articleName" : dup.articleName, "status" : dup.status}
        
        # Insert in bulk
        result = None
        try:
            result = self._bulkInsert(dbName, colName, [article.__dict__ for article in articles], update = update)
            if update:
                response = Abstracts.Response(result, isSuccess=True, returnValue=1)
            else: 
                response = Abstracts.Response("Added %d articles to database" % len(result.inserted_ids), isSuccess=True, returnValue=len(result.inserted_ids))
            outputQueue.put(response)
            if colName is "pending":
                self._simpleBlockingInvocation(self, "MongoDB_createLog", dbName = dbName, logMessage = "Added %d articles to %s collection: %s" % (len(result.inserted_ids), colName, [article.articleID for article in articles]))  
        except TypeError as te:
            print(te)
            response = Abstracts.Response("TypeError: The given article list is invalid to be added to MongoDB")
            outputQueue.put(response)
            return
        except pymongo.errors.BulkWriteError as bwe:
            numInserted = bwe.details["nInserted"]
            response = Abstracts.Response("BulkWriteError: Some articles are not inserted due to duplication. Details: %s" % bwe.details, isSuccess=False, returnValue=numInserted)
            outputQueue.put(response)
            return
         
    def _createLog(self, inputQueue, outputQueue, dbName, logMessage):
        db, col = self._getDBCol(dbName, self.colLog)
        try:
            log = {"time" : str(datetime.datetime.now()), "message" : logMessage}
            col.insert_one(log)
            response = Abstracts.Response("Added log \"" + logMessage + "\".", isSuccess=True)
            outputQueue.put(response)
        except TypeError:
            response = Abstracts.Response("TypeError: The given logMessage is invalid to be added to MongoDB")
            outputQueue.put(response)
            pass
        return
    
    def _createTopic(self, inputQueue, outputQueue, topic, force = False):
        dbName = topic.topicID
        db, col = self._getDBCol(dbName, self.colTopic)
        # Check whether the given topic already exists in the database
        existingTopics = [t for t in col.find({"topicID" : dbName})]
        duplicated = len(existingTopics) > 0
        if duplicated and not force:
            response = Abstracts.Response("The given topic already exists in the database. Use force option to update.")
            outputQueue.put(response)
            return
        try:
            insertData = topic.__dict__
            del(insertData["entityType"])
            insertData["creationTime"] = str(datetime.datetime.now())
            col.insert_one(topic.__dict__)
            if not duplicated:
                response = Abstracts.Response("Inserted topic to the database", isSuccess=True)
                self._simpleBlockingInvocation(self, "MongoDB_createLog", dbName = dbName, logMessage = "Created topic [%s]" % insertData["topicName"])
            else:
                response = Abstracts.Response("Updated the topic", isSuccess=True)
                self._simpleBlockingInvocation(self, "MongoDB_createLog", dbName = dbName, logMessage = "Updated topic")
            outputQueue.put(response)
        except TypeError:
            response = Abstracts.Response("TypeError: The given topic is not valid")
            outputQueue.put(response)
            return
    
    def _readArticles(self, inputQueue, outputQueue, dbName, collection, query):
        # Check the type of collection to insert
        colName = ""
        if collection is "pending":
            colName = self.colPending
        elif collection is "accepted" :
            colName = self.colAccepted
        elif collection is "rejected":
            colName = self.colRejected
        else:
            response = Abstracts.Response("The collection must be one of [pending, accepted, rejected]")
            outputQueue.put(response)
        
        # Read articles from database
        try:
            results = self._readFromDB(dbName, colName, query)
            response = Abstracts.Response("Readed articles from %s" % colName, isSuccess=True, returnValue=results)
            outputQueue.put(response)
        except TypeError:
            response = Abstracts.Response("TypeError: The given query is invalid.")
            outputQueue.put(response)
           
    def _findPossibleDuplicates(self, inputQueue, outputQueue, dbName, collection, articles):
        # Check the type of collection to insert
        colName = ""
        if collection is "pending":
            colName = self.colPending
        elif collection is "accepted" :
            colName = self.colAccepted
        elif collection is "rejected":
            colName = self.colRejected
        else:
            response = Abstracts.Response("The collection must be one of [pending, accepted, rejected]")
            outputQueue.put(response)
            
        # Check each article in the list for duplication
        dupCount= 0
        for article in articles:
            duplicate = self._readFromDBFuzzy(dbName, colName, article.articleName)
            if duplicate:
                article.addDuplicate(duplicate)
                dupCount += 1
            else:
                continue
            
        if dupCount == 0:
            response = Abstracts.Response("No duplicates detected.", returnValue = articles)
            outputQueue.put(response)
        else:
            response = Abstracts.Response("%d possible duplicates detected" % dupCount, isSuccess=True, returnValue=articles)
            outputQueue.put(response)
        return
            
    def _readLogs(self, inputQueue, outputQueue, dbName, query):
        try:
            results = self._readFromDB(dbName, self.colLog, query)
            response = Abstracts.Response("Retrieved %d logs" % len(results), isSuccess=True, returnValue=results)
            outputQueue.put(response)
        except TypeError:
            response = Abstracts.Response("TypeError: The given query is invalid.")
            outputQueue.put(response)
            
    def _readTopic(self, inputQueue, outputQueue, dbName, query = None):
        try:
            result = self._readFromDB(dbName, self.colTopic, query)
            topic = Abstracts.Topic(str(result["topicID"]), str(result["topicName"]), str(result["databaseProvider"]),
                            result["primarySearchProviders"], result["snowballingSearchProviders"])
            response = Abstracts.Response("Retrieved topic %s" % result, isSuccess=True, returnValue=topic)
            outputQueue.put(response)
        except TypeError:
            response = Abstracts.Response("TypeError: The given query is invalid.")
            outputQueue.put(response)
            
    def _acceptArticles(self, inputQueue, outputQueue, dbName, articleIDs):
        query = {"articleID" : {"$in" : articleIDs}}
        # Get requested articles from pending collection
        res = self._simpleBlockingInvocation(self, "MongoDB_readArticles", dbName = dbName, collection = "pending", query = query)
        pendingArticles = res.returnValue
        if len(pendingArticles) == 0:
            response = Abstracts.Response("Cannot find articles with specified IDs")
            outputQueue.put(response)
            return
        else:
            # Change the status of articles to accepted
            for article in pendingArticles:
                article.status = "ACCEPTED"
            # Move selected pending articles to accepted collection
            movingResult = self._simpleBlockingInvocation(self, "MongoDB_createArticles", dbName = dbName, collection = "accepted", articles = pendingArticles)
            self._simpleBlockingInvocation(self, "MongoDB_removeArticles", dbName = dbName, collection = "pending", articleIDs = articleIDs)
            # If duplication is detected when inserting, report differently
            if not movingResult.isSuccess:
                numInserted = movingResult.returnValue
                if not numInserted == 0:
                    self._simpleBlockingInvocation(self, "MongoDB_createLog", dbName = dbName, logMessage = "Accepted %d articles (The following list presents all accepted articles, including duplications): %s" 
                                           % (numInserted, [article.articleID for article in pendingArticles]))
                    response = Abstracts.Response("Moved %d articles to accepted collection. Some articles already exist" % numInserted, isSuccess=True, returnValue=numInserted)
                    outputQueue.put(response)
                else:
                    response = Abstracts.Response("All selected articles are already in the accepted collection")
                    outputQueue.put(response)
            else:
                self._simpleBlockingInvocation(self, "MongoDB_createLog", dbName = dbName, logMessage = "Accepted %d articles: %s" 
                                           % (len(pendingArticles), [article.articleID for article in pendingArticles]))
                response = Abstracts.Response("Moved %d articles to accepted collection" % len(pendingArticles), isSuccess=True, returnValue=len(pendingArticles))
                outputQueue.put(response)        
        # Move other papers in pending to the rejected collection
        
    def _removePendingArticles(self, inputQueue, outputQueue, dbName):
        res = self._simpleBlockingInvocation(self, "MongoDB_readArticles", dbName = dbName, collection = "pending", query = {})
        if not res.isSuccess:
            response = Abstracts.Response("Error encountered while retrieving pending articles from database: %s" % res.responseMessage)
            outputQueue.put(response)
            return
        
        self._simpleBlockingInvocation(self, "MongoDB_removeArticles", dbName = dbName, collection = "pending", articleIDs = [x.articleID for x in res.returnValue])
        response = Abstracts.Response("Finished operation.", isSuccess=True)
        outputQueue.put(response)
        return
    
        
if __name__ == "__main__":
    # Registry
    registry = Abstracts.AbsRegistry(moduleParams = {}, dependencies = {}, registry = None)
    registry.start()
    
    connInfo = {"host" : "localhost","port" : 27017,
                "col" : {"PENDING" : "pending", "ACCEPTED" : "accepted", "REJECTED" : "rejected", "LOG" : "log", "TOPIC" : "topic"}}
    dbProvider = MongoDBProvider(moduleParams = {"connInfo" : connInfo}, dependencies = [],
                                 registry = registry)
    dbProvider.start()
    inputQueue = Queue.Queue()
    outputQueue = Queue.Queue()
    articles = []
    for i in range(5):
        articles.append(Abstracts.Article("a%d" % i, "Test Article %d" % i, {}, "http://repository.com"))
    articleIDs = [article.articleID for article in articles]
    dbProvider.addRequest("MongoDB_createArticles", inputQueue, outputQueue, dbName = "LiSS", collection = "pending", articles = articles)
#    dbProvider.addRequest("MongoDB_acceptArticles", inputQueue, outputQueue, dbName = "LiSS", articleIDs = ["a4"])
#    dbProvider.addRequest("MongoDB_removeArticles", inputQueue, outputQueue, dbName = "LiSS", collection = "accepted", articleIDs = ["a4"])    
#    dbProvider.addRequest("MongoDB_findPossibleDuplicates", inputQueue, outputQueue, dbName = "LiSS", collection = "pending", articleName = "article 1")    
    
#    print(outputQueue.get(timeout=2).responseMessage)
    print(outputQueue.get(timeout=2).responseMessage)
#    time.sleep(5)
